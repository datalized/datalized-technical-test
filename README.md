# Prueba Técnica
### Data Scientist Consultor BI/ML

### Descripción del caso

En esta prueba técnica queremos evaluar tu capacidad de tomar un conjunto de datos en bruto y traducirlo en uno o dos insights claros a través de una visualización.

A continuación se adjunta un archivo .jl (JSON lines) con un subconjunto de información asociada a publicaciones de propiedades en Portal Inmobiliario para la Región Metropolitana. Esta información corresponde al estado de las publicaciones el día 11 de septiembre de 2019.

Este archivo considera información como el precio de la publicación, el tipo de publicación, el tipo de vivienda y su ubicación geográfica. Cada fila corresponde a una publicación.

### ¿Qué esperamos de ti?

Con esta información esperamos que contestes, dentro de lo posible, la siguiente pregunta:

**¿Cuál es el estado actual del mercado de arriendos en Stgo? ¿Recomendarías comprar o arrendar?**

Para resolver el caso deberas:

* Procesar el archivo .jl para dejarlo en un formato que te permita trabajar.
* Desarrollar una vista (en alguna herramienta de visualización, como Tableau o PowerBI) que te permita contestar la pregunta en la manera más sencilla posible.
* Documentar, en no más de una plana, como procesaste el archivo .jl, los supuesto que tomaste y como enfrentaste la vista para contestar la pregunta.
* Compartir la vista que desarrollaste en el formato que más te acomode (embeddeado en una web o como imagen/pdf) y cualquier otro archivo que te parezca relevante compartir.

El desarrollo de esta solución debiese tomar entre 1 y 2 horas.

*Ante cualquier duda escribir a matias@datalized.cl*